/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.seed.ListaCD;

/**
 *
 * @author docente
 */
public class TestListaCD {

    public static void main(String[] args) {

//        ListaCD<Integer> lista = new ListaCD();
//        lista.insertarInicio(5);
//        lista.insertarInicio(3);
//        lista.insertarInicio(9);
//        System.out.println(lista.toString());
//        
//        ListaCD<Integer> lista2 = new ListaCD();
//        lista2.insertarFinal(5);
//        lista2.insertarFinal(3);
//        lista2.insertarFinal(9);
//        System.out.println(lista2.toString());   
//        System.out.println(lista2.containTo(9)); 
//        System.out.println(lista2.containTo(20)); 
        //test 2a
        ListaCD<String> l1 = new ListaCD<>();
        ListaCD<String> l2 = new ListaCD<>();

        //l1
        l1.insertarInicio("A");
        l1.insertarInicio("B");
        l1.insertarInicio("A"); //
        l1.insertarInicio("C");
        l1.insertarInicio("D");

        //l2
        l2.insertarInicio("X");
        l2.insertarInicio("Y");
        l2.insertarInicio("Z");

        System.out.println("Lista l1: " + l1);
        // Comprobar si la lista l1 contiene elementos repetidos en sí misma
        boolean contieneRepetidosL1 = l1.contieneElemRepetidos();

        System.out.println("Lista l2: " + l2);
        // Comprobar si la lista l2 contiene elementos repetidos en sí misma
        boolean contieneRepetidosL2 = l2.contieneElemRepetidos();

        if (contieneRepetidosL1) {
            System.out.println("La lista l1 contiene elementos repetidos en sí misma.");
        } else {
            System.out.println("La lista l1 no contiene elementos repetidos en sí misma.");
        }

        if (contieneRepetidosL2) {
            System.out.println("La lista l2 contiene elementos repetidos en sí misma.");
        } else {
            System.out.println("La lista l2 no contiene elementos repetidos en sí misma.");
        }

    }

}
