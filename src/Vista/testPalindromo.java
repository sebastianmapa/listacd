/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Vista;

import Util.seed.ListaS;

/**
 *
 * @author SEBASTIAN
 */
public class testPalindromo {

    public void testEsPalindromo() {
        // Caso de prueba 1: Lista vacía (palíndromo)
        ListaS<String> lista1 = new ListaS<>();
        boolean result1 = lista1.esPalindromo();
        System.out.println("Caso 1: " + result1);

        // Caso de prueba 2: Lista con un elemento (palíndromo)
        ListaS<Integer> lista2 = new ListaS<>();
        lista2.insertarInicio(1);
        boolean result2 = lista2.esPalindromo();
        System.out.println("Caso 2: " + result2);

        // Caso de prueba 3: Lista no palíndromo
        ListaS<String> lista3 = new ListaS<>();
        lista3.insertarInicio("A");
        lista3.insertarFin("B");
        boolean result3 = lista3.esPalindromo();
        System.out.println("Caso 3: " + result3);

        // Caso de prueba 4: Lista palíndromo
        ListaS<String> lista4 = new ListaS<>();
        lista4.insertarInicio("A");
        lista4.insertarFin("A");
        lista4.insertarFin("B");
        lista4.insertarFin("A");
        lista4.insertarFin("A");
        boolean result4 = lista4.esPalindromo();
        System.out.println("Caso 4: " + result4);

        // Caso de prueba 5: Lista palíndromo con más elementos
        ListaS<Integer> lista5 = new ListaS<>();
        lista5.insertarInicio(1);
        lista5.insertarFin(2);
        lista5.insertarFin(1);
        lista5.insertarFin(2);
        lista5.insertarFin(1);
        boolean result5 = lista5.esPalindromo();
        System.out.println("Caso 5: " + result5);

        // Caso de prueba 6: Lista no palíndromo con más elementos
        ListaS<Integer> lista6 = new ListaS<>();
        lista6.insertarInicio(1);
        lista6.insertarFin(2);
        lista6.insertarFin(3);
        boolean result6 = lista6.esPalindromo();
        System.out.println("Caso 6: " + result6);
    }

    public static void main(String[] args) {
        testPalindromo test = new testPalindromo();
        test.testEsPalindromo();
    }
}
