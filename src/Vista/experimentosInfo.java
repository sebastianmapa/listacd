/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Vista;

import Util.seed.ListaS;
import java.util.Random;

/**
 *
 * @author SEBASTIAN
 */
public class experimentosInfo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ListaS<Integer> lista = new ListaS<>();
        Random random = new Random();

        // enteros del 1 al 5000000 en orden aleatorio
        for (int i = 1; i <= 5; i++) {
            lista.insertarInicio(i); // Agrega al inicio de la lista
        }

        // Mezcla la lista
        for (int i = 0; i < lista.getSize(); i++) {
            int randomIndex = random.nextInt(lista.getSize());
            int temp = lista.get(i);
            lista.set(i, lista.get(randomIndex));
            lista.set(randomIndex, temp);
        }

        System.out.println("Lista desordenada:" + lista);
        lista.ordenarInsercionPorInfos(lista);
        System.out.println("Lista ordenada:" + lista);
    }
}
