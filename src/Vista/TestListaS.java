/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.seed.ListaS;

/**
 *
 * @author Docente
 */
public class TestListaS {

    /**
     * YO SOY EL DESARROLLADOR QUE USA LA ESTRUCTURA
     *
     * @param args
     */
    public static void main(String[] args) {
        ListaS<Integer> l = new ListaS<>();
//        ListaS<Integer> listaPorAgregar = new ListaS(); // Crea una nueva lista para agregar

        l.insertarInicio(0);
        l.insertarInicio(1);
        l.insertarInicio(5);
        l.insertarInicio(8);
        l.insertarInicio(2);


        System.out.println("Lista l original: " + l.toString());
        
        //metodo insertarPorInfos
        l.ordenarInsercionPorInfos(l);
        System.out.println("Lista ordenada: " + l);
        
        // Agrega elementos a la lista listaPorAgregar
//        listaPorAgregar.insertarInicio(10);
//        listaPorAgregar.insertarFin(15);
//        listaPorAgregar.insertarFin(20);

//        System.out.println("Lista lista por Agregar : " + listaPorAgregar.toString());

        // Llama al método addAll para agregar los elementos de listaPorAgregar a la lista l
//        boolean agregado = l.addAll(2, listaPorAgregar);

//        if (agregado) {
//            System.out.println("Lista l después de addAll: " + l.toString());
//            System.out.println("lista por agregar despues addAll: " + listaPorAgregar.toString());
//        } else {
//            System.out.println("No se agregaron elementos a la lista l.");
//        }

        // Realiza otras operaciones en la lista l (por ejemplo, elevar al cuadrado)
//        for (int i = 0; i < l.getSize(); i++) {
//            int cuadrado = l.get(i);
//            cuadrado *= cuadrado;
//            l.set(i, cuadrado);
//        }
//        System.out.println("Lista l después de elevar al cuadrado: " + l.toString());
        // Intenta acceder a un índice fuera de rango (propiciará un error)
//        try {
//            System.out.println(l.get(100));
//        } catch (Exception e) {
//            System.err.println("Error: " + e.getMessage());
//        }

    }
}
