/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Util.seed;

/**
 *
 * @author docente
 */
public class ListaCD<T> {

    private NodoD<T> cabeza;
    private int size;

    public ListaCD() {

        this.cabeza = new NodoD(null, null, null);
        this.cabeza.setAnt(cabeza);
        this.cabeza.setSig(cabeza);
        this.size = 0;

    }

    public void insertarInicio(T info) {

        NodoD temp = new NodoD(info, this.cabeza.getSig(), this.cabeza);
        this.cabeza.setSig(temp);
        temp.getSig().setAnt(temp);
        this.size++;

    }

    public void insertarFinal(T info) {

        NodoD temp = new NodoD(info, this.cabeza, this.cabeza.getAnt());
        this.cabeza.setAnt(temp);
        temp.getAnt().setSig(temp);
        this.size++;

    }

    public boolean containTo(T info) {

        for (NodoD<T> temp = this.cabeza.getSig(); temp != this.cabeza; temp = temp.getSig()) {

            if (temp.getInfo().equals(info)) {

                return true;

            }

        }

        return false;

    }

    public String toString() {

        String msg = "";

        for (NodoD<T> temp = this.cabeza.getSig(); temp != this.cabeza; temp = temp.getSig()) {

            msg += temp.toString() + "\t";

        }

        return msg;

    }

    public boolean contieneElemRepetidos() {
        NodoD<T> nodoAct = this.cabeza.getSig();
        while (nodoAct != this.cabeza) {
            T info = nodoAct.getInfo();
            NodoD<T> nodoComparar = this.cabeza.getSig();
            while (nodoComparar != nodoAct) {
                if (info.equals(nodoComparar.getInfo())) {
                    return true; 
                }
                nodoComparar = nodoComparar.getSig();
            }
            nodoAct = nodoAct.getSig();
        }
        return false;
    }
    
//    public void eliminarElemRepetidos(){
//        
//    }

}
