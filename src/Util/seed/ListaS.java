/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Util.seed;

/**
 *
 * @author Docente
 */
public class ListaS<T> {

    private Nodo<T> cabeza;
    private int size;

    public ListaS() {
        this.cabeza = null; //no es necesario
        this.size = 0;
    }

    public void insertarInicio(T info) {
        Nodo<T> nuevo = new Nodo(info, this.cabeza);
        this.cabeza = nuevo;
        this.size++;

    }

    public void insertarFin(T info) {
        if (this.isEmpty()) {
            this.insertarInicio(info);
        } else {
            Nodo<T> x = getPos(this.size - 1); //mètodo encuentra la direcciòn de memoria del nodo que està de ùltimo
            Nodo<T> nuevo = new Nodo(info, null);
            x.setSig(nuevo);
            this.size++;
        }
    }

    public T get(int i) {
        return this.getPos(i).getInfo();
    }

    public void set(int i, T info) {
        this.getPos(i).setInfo(info);
    }

    private Nodo<T> getPos(int pos) {
        if (this.isEmpty()) {
            throw new RuntimeException("Lista vacìa, no hay posiciones");
        }
        if (pos < 0 || pos >= this.size) {
            throw new RuntimeException("Posición fuera de rango, esta debe ser entre:0 y " + (this.size - 1));
        }

        Nodo<T> temp = this.cabeza;
        //int i=0;
        while (pos > 0) //i<pos
        {
            temp = temp.getSig();
            pos--; //i++
        }
        return temp;
    }

    @Override
    public String toString() {
        String msg = "";
        for (Nodo<T> x = this.cabeza; x != null; x = x.getSig()) {
            msg += x.toString() + "->";
        }
        return "cab->" + msg + "null\n Contiene:" + this.size + " elementos";
    }

    public int getSize() {
        return size;
    }

    public boolean isEmpty() {
        return this.size == 0;
    }

    public void insertarOrdenado(T info) {
        if (this.isEmpty()) {
            this.insertarInicio(info);
        } else {
            Nodo<T> x = this.cabeza;
            Nodo<T> y = x;
            while (x != null) {
                Comparable comparador = (Comparable) info;
                int rta = comparador.compareTo(x.getInfo());
                if (rta < 0) {
                    break;
                }
                y = x;
                x = x.getSig();
            }
            if (x == y) {
                this.insertarInicio(info);
            } else {
                y.setSig(new Nodo(info, x));
                this.size++;
            }
        }
    }

    /**
     * metodo para eliminar alguna lista
     *
     */
    public void eliminarLista() {
        this.cabeza = null;
        this.size = 0;
    }

    /**
     * retorna verdadero si se ha añadido algo a la lista actual(no a la del
     * parametro)
     *
     */
    public boolean addAll(int index, ListaS<T> listaPorAgregar) {
        if (listaPorAgregar == null || listaPorAgregar.isEmpty()) {
            return false; // La listaPorAgregar está vacía o es nula, no se realiza ninguna inserción o modificación.
        }

        if (index < 0 || index > this.size) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + this.size);
        }

        if (index == 0) {
            // Insertar al principio de la lista actual
            listaPorAgregar.getPos(listaPorAgregar.size - 1).setSig(this.cabeza);
            this.cabeza = listaPorAgregar.cabeza;
        } else {
            // Insertar en una posición diferente de la lista actual
            Nodo<T> anterior = this.getPos(index - 1); // Nodo anterior en la lista actual
            Nodo<T> siguiente = anterior.getSig(); // Nodo siguiente en la lista actual

            anterior.setSig(listaPorAgregar.cabeza);
            listaPorAgregar.getPos(listaPorAgregar.size - 1).setSig(siguiente);
        }

        // Actualizar el tamaño de la lista
        listaPorAgregar.eliminarLista();
        this.size += listaPorAgregar.size;

        return true;
    }

    public boolean addAll(ListaS<T> listaPorAgregar) {
        if (listaPorAgregar == null || listaPorAgregar.isEmpty()) {
            return false; // La listaPorAgregar está vacía o es nula, no se realiza ninguna inserción o modificación.
        }
        Nodo<T> anterior = this.getPos(size - 1);
        anterior.setSig(listaPorAgregar.cabeza);

        // Actualizar el tamaño de la lista
        listaPorAgregar.eliminarLista();
        this.size += listaPorAgregar.size;

        return true;
    }

    /**
     * Elimina y retorna el elemento en la posición especificada.
     *
     * @param index La posición del elemento a eliminar.
     * @return El elemento eliminado.
     * @throws RuntimeException Si la lista está vacía o la posición está fuera de rango.
     */
    public T remove(int index) {
        Nodo<T> x = null;
        Nodo<T> y = null;

        if (this.isEmpty()) {
            throw new RuntimeException("imposible borrar lista vacia");
        }

        if (index < 0 || index >= this.getSize()) {
            throw new RuntimeException("fuera de rango");
        }

        if (index == 0) {
            x = this.cabeza;
            this.cabeza = cabeza.getSig();

        } else {
            y = this.getPos(index - 1);
            x = y.getSig();
            y.setSig(x.getSig());

        }
        this.size -= 1;
        x.setSig(null);
        return x.getInfo();

    }

    public void ordenarInsercionPorInfos(ListaS<T> lista) {
        if (lista.isEmpty() || lista.getSize() == 1) {
            return; // lista vacía u ordenada
        }

        Nodo<T> nuevaCabeza = null;
        Nodo<T> actual = lista.cabeza;

        while (actual != null) {
            Nodo<T> siguiente = actual.getSig();

            if (nuevaCabeza == null || ((Comparable<T>) actual.getInfo()).compareTo(nuevaCabeza.getInfo()) <= 0) {
                // el nodo actual debe ir al principio de la lista
                actual.setSig(nuevaCabeza);
                nuevaCabeza = actual;
            } else {
                Nodo<T> temp = nuevaCabeza;
                while (temp.getSig() != null && ((Comparable<T>) actual.getInfo()).compareTo(temp.getSig().getInfo()) > 0) {
                    temp = temp.getSig();
                }
                // se inserta el nodo actual en su posición ordenada
                actual.setSig(temp.getSig());
                temp.setSig(actual);
            }
//            if (actual != null) {
//                System.out.println("Acutual: " + actual.getInfo());
//            }
//
//            if (nuevaCabeza != null) {
//                System.out.println("NC " + nuevaCabeza.getInfo());
//            }
            actual = siguiente;

        }
        lista.cabeza = nuevaCabeza; // se actaliza la cabeza de la lista original
    }

    public T desplazarAlFianl(int index) {
        Nodo<T> tmp = null;
        Nodo<T> ultimo = this.getPos(size - 1);
        if (index < 0 || index > this.size) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + this.size);
        }
        if (index == this.size - 1) {
            return this.get(index);

        }
        tmp = this.getPos(index);

        this.remove(index);
        ultimo.setSig(tmp);
        tmp.setSig(null);
        this.size++;

        return tmp.getInfo();

    }

    public boolean contieneElemento(T elementoBuscado) {
        Nodo<T> nodoActual = cabeza;
        while (nodoActual != null) {
            if (elementoBuscado.equals(nodoActual.getInfo())) {
                return true; // Elemento encontrado
            }
            nodoActual = nodoActual.getSig();
        }
        return false; // Elemento no encontrado
    }

    /**
     * Verifica si la lista contiene elementos repetidos.
     *
     * @return true si la lista contiene elementos repetidos, de lo contrario, false.
     */
    public boolean contieneElemRepetidos() {
        Nodo<T> nodoActual = this.cabeza;
        while (nodoActual != null) {
            Nodo<T> nodoSiguiente = nodoActual.getSig();
            while (nodoSiguiente != null) {
                if (sonElementosIguales(nodoActual.getInfo(), nodoSiguiente.getInfo())) {
                    return true;
                }
                nodoSiguiente = nodoSiguiente.getSig();
            }
            nodoActual = nodoActual.getSig();
        }
        return false;
    }

    private boolean sonElementosIguales(T elemento1, T elemento2) {
        return elemento1.equals(elemento2);
    }

    /**
     * Verifica si la lista no contiene elementos repetidos.
     *
     * @return true si la lista no contiene elementos repetidos, de lo contrario, false.
     */
    public boolean noContieneElemRepetidos() {
        Nodo<T> nodoActual = this.cabeza;
        int tamaño = this.getSize();
        for (int i = 0; i < tamaño; i++) {
            if (!contieneElemento(nodoActual.getInfo())) {
                return true;
            }
            nodoActual = nodoActual.getSig();
        }
        return false;
    }

    /**
     * Copia la lista actual a una nueva lista y la retorna.
     *
     * @return Una nueva lista que es una copia de la lista actual.
     * @throws RuntimeException Si la lista actual está vacía.
     */
    public ListaS copiarLista() {
        if (this.isEmpty()) {
            throw new RuntimeException("la lista esta vacia");
        }
        ListaS<T> copia = new ListaS();
        Nodo<T> actual = this.cabeza;
        while (actual != null) {
            copia.insertarFin(actual.getInfo());
            actual = actual.getSig();
        }
        return copia;

    }

    /**
     * Verifica si la lista es un palíndromo.
     *
     * @return true si la lista es un palíndromo, de lo contrario, false.
     */
    public boolean esPalindromo() {
        int tamaño = this.getSize();
        if (tamaño <= 1) {
            return true; // Las listas con 0 o 1 elemento siempre son palíndromos
        }

        Nodo<T> nodoActual = this.cabeza;
        Nodo<T> nodoFinal = this.getPos(tamaño - 1);

        for (int i = 0; i < tamaño / 2; i++) {
            if (!nodoActual.getInfo().equals(nodoFinal.getInfo())) {
                return false; // No es un palíndromo
            }
            nodoActual = nodoActual.getSig();
            nodoFinal = this.getPos(tamaño - 2 - i);
        }

        return true; // Es un palíndromo
    }
    
    /**
     * Invierte la lista en su lugar.
     *
     * @throws RuntimeException Si la lista está vacía.
     */
    public void invertirLista() {
        if (this.isEmpty() || this.cabeza == null) {
            throw new RuntimeException("No se puede invertir una lista vacía");
        }

        Nodo<T> actual = this.cabeza;
        Nodo<T> anterior = null;
        Nodo<T> siguiente = null;

        while (actual != null) {
            siguiente = actual.getSig();
            actual.setSig(anterior);
            anterior = actual;
            actual = siguiente;
        }

        this.cabeza = anterior;
    }

    //Método para ordenar con el método de la burbuja
    private static void ordenar(ListaS<Integer> l1) {
        int n = l1.getSize();

        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (l1.get(j).compareTo(l1.get(j + 1)) > 0) {
                    int temp = l1.get(j);
                    l1.set(j, l1.get(j + 1));
                    l1.set(j + 1, temp);
                }
            }
        }
    }

    public void ordenarInsercionPorNodos(ListaS<T> lista) {

    }
}
